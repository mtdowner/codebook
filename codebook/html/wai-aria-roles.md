# 🩹 WAI-ARIA Roles

ARIA roles provide semantic meaning to content, allowing screen readers and other tools to present and support interaction with an object in a way that is consistent with user expectations of that type of object. ARIA roles can be used to describe elements that don't natively exist in HTML or exist but don't yet have full browser support.

By default, many semantic elements in HTML have a role; for example, `<input type="radio">` has the "radio" role. Non-semantic elements in HTML do not have a role; `<div>` and `<span>` without added semantics return `null`. The `role` attribute can provide semantics.

ARIA roles are added to HTML elements using `role="role type"`, where _role type_ is the name of a role in the ARIA specification. Some roles require the inclusion of associated ARIA states or properties; others are only valid in association with other roles.

For example, `<ul role="tabpanel">` will be announced as a 'tab panel' by screen readers. However, if the tab panel doesn't have nested tabs, the element with the `tabpanel` role is not in fact a tab panel and accessibility has actually been negatively impacted.

The [ARIA states and properties](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes) associated with each role are included in the role's pages, with each attribute also having a dedicated page.

### [ARIA role types](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#aria\_role\_types) <a href="#aria_role_types" id="aria_role_types"></a>

There are 6 categories of ARIA roles:

#### [1. Document structure roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#1.\_document\_structure\_roles) <a href="#id-1._document_structure_roles" id="id-1._document_structure_roles"></a>

Document Structure roles are used to provide a structural description for a section of content. Most of these roles should no longer be used as browsers now support semantic HTML element with the same meaning. The roles without HTML equivalents, such as presentation, toolbar and tooltip roles, provide information on the document structure to assistive technologies such as screen readers as equivalent native HTML tags are not available.

* [toolbar](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/toolbar\_role)
* [tooltip](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tooltip\_role)
* [feed](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/feed\_role)
* [math](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/math\_role)
* [presentation](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/presentation\_role) / [none](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/none\_role)
* [note](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/note\_role)

For most document structure roles, semantic HTML equivalent elements are available and supported. Avoid using:

* [application](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/application\_role)
* [article](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/article\_role) (use [`<article>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/article))
* [cell](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/cell\_role) (use [`<td>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td))
* [columnheader](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/columnheader\_role) (use [`<th scope="col">`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th))
* [definition](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/definition\_role) (use [`<dfn>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dfn))
* [directory](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/directory\_role)
* [document](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/document\_role)
* [figure](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/figure\_role) (use [`<figure>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figure) instead)
* [group](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/group\_role)
* [heading](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/heading\_role) (use [h1](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading\_Elements) thru [h6](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading\_Elements))
* [img](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/img\_role) (use [`<img>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img) or [`<picture>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture) instead)
* [list](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/list\_role) (use either [`<ul>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul) or [`<ol>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol) instead)
* [listitem](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/listitem\_role) (use [`<li>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li) instead)
* [meter](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/meter\_role) (use [`<meter>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meter) instead)
* [row](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/row\_role) (use the [`<tr>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr) with [`<table>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table))
* [rowgroup](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/rowgroup\_role) (use [`<thead>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead), [`<tfoot>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tfoot) and [`<tbody>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody))
* [rowheader](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/rowheader\_role) (use [`<th scope="row">`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th))
* [separator](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/separator\_role) (use [`<hr>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/hr) if it doesn't have focus)
* [table](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/table\_role) (use [`<table>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table))
* [term](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/term\_role) (use [`<dfn>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dfn))

These are included for completeness, but in most cases are rarely, if ever, useful:

* [`associationlist`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`associationlistitemkey`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`associationlistitemvalue`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`blockquote`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`caption`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`code`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`deletion`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`emphasis`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`insertion`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`paragraph`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`strong`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`subscript`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`superscript`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)
* [`time`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)

#### [2. Widget roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#2.\_widget\_roles)

The various widget role are used to define common interactive patterns. Similar to the document structure roles, some of these roles duplicate the semantics of native HTML elements that are well supported, and should not be used. The difference between the two lists is that, generally, the widget roles require JavaScript interaction and the document structure roles don't necessarily.

* [scrollbar](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/scrollbar\_role)
* [searchbox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/searchbox\_role)
* [separator](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/separator\_role) (when focusable)
* [slider](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/slider\_role)
* [spinbutton](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/spinbutton\_role)
* [switch](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/switch\_role)
* [tab](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tab\_role)
* [tabpanel](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tabpanel\_role)
* [treeitem](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treeitem\_role)

### **Composite widget roles**

* [combobox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/combobox\_role)
* [menu](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menu\_role)
* [menubar](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menubar\_role)
* [tablist](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tablist\_role)
* [tree](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tree\_role)
* [treegrid](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treegrid\_role)

#### [3. Landmark roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#3.\_landmark\_roles) <a href="#id-3._landmark_roles" id="id-3._landmark_roles"></a>

Landmark roles provide a way to identify the organization and structure of a web page. By classifying and labeling sections of a page, structural information conveyed visually through layout is represented programmatically. Screen readers use landmark roles to provide keyboard navigation to important sections of a page. Use these sparingly. Too many landmark roles create "noise" in screen readers, making it difficult to understand the overall layout of the page.

* [banner](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/banner\_role) (document [`<header>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/header))
* [complementary](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/complementary\_role) ([`<aside>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/aside))
* [contentinfo](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/contentinfo\_role) (document [`<footer>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/footer))
* [form](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/form\_role) ([`<form>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form))
* [main](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/main\_role) ([`<main>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main))
* [navigation](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/navigation\_role) ([`<nav>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/nav))
* [region](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/region\_role) ([`<section>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section))
* [search](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/search\_role)

#### [4. Live region roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#4.\_live\_region\_roles) <a href="#id-4._live_region_roles" id="id-4._live_region_roles"></a>

Live Region roles are used to define elements with content that will be dynamically changed. Sighted users can see dynamic changes when they are visually noticeable. These roles help low vision and blind users know if content has been updated. Assistive technologies, like screen readers, can be made to announce dynamic content changes:

* [alert](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/alert\_role)
* [log](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/log\_role)
* [marquee](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/marquee\_role)
* [status](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/status\_role)
* [timer](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/timer\_role)

#### [5. Window roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#5.\_window\_roles) <a href="#id-5._window_roles" id="id-5._window_roles"></a>

Window roles define sub-windows to the main document window, within the same window, such as pop up modal dialogs:

* [alertdialog](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/alertdialog\_role)
* [dialog](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/dialog\_role)

#### [6. Abstract roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#6.\_abstract\_roles) <a href="#id-6._abstract_roles" id="id-6._abstract_roles"></a>

Abstract roles are only intended for use by browsers to help organize and streamline a document. They should not be used by developers writing HTML markup. Doing so will not result in any meaningful information being conveyed to assistive technologies or to users.

Avoid using [command](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/command\_role), [composite](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/composite\_role), [input](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/input\_role), [landmark](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/landmark\_role), [range](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/range\_role), [roletype](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/roletype\_role), [section](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/section\_role), [sectionhead](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/sectionhead\_role), [select](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/select\_role), [structure](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structure\_role), [widget](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/widget\_role), and [window](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/window\_role).

Avoid using [button](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/button\_role), [checkbox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/checkbox\_role), [gridcell](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/gridcell\_role), [link](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/link\_role), [menuitem](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menuitem\_role), [menuitemcheckbox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menuitemcheckbox\_role), [menuitemradio](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menuitemradio\_role), [option](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/option\_role), [progressbar](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/progressbar\_role), [radio](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/radio\_role), and [textbox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/textbox\_role).

Avoid using [grid](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/grid\_role), [listbox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/listbox\_role), and [radiogroup](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/radio\_role).

{% hint style="info" %}
<mark style="color:purple;">**Note**</mark> <mark style="color:purple;"></mark><mark style="color:purple;">that there is also a widget role (</mark><mark style="color:purple;">`role="widget"`</mark><mark style="color:purple;">), which is an abstract role and not in the widget role category.</mark>
{% endhint %}

### Definitions

[`ARIA: alert role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/alert\_role)

important, and usually time-sensitive, information; type of [`status`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/status\_role) processed as an atomic live region.

[`ARIA: alertdialog role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/alertdialog\_role)

used on modal alert dialogs that interrupt a user's workflow to communicate an important message and require a response.

[`ARIA: application role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/application\_role)

indicates to assistive technologies that an element _and all of its children_ should be treated similar to a desktop application, and no traditional HTML interpretation techniques should be used; should only be used to define very dynamic and desktop-like web applications; most mobile and desktop web apps _are not_ considered applications for this purpose.

[`ARIA: article role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/article\_role)

indicates a section of a page that could easily stand on its own on a page, in a document, or on a website. It is usually set on related content items such as comments, forum posts, newspaper articles or other items grouped together on one page.

[`ARIA: banner role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/banner\_role)

a global site header, which usually includes a logo, company name, search feature, and possibly the global navigation or a slogan; generally located at the top of the page.

[`ARIA: button role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/button\_role)

clickable elements that trigger a response when activated by the user; adding `role="button"` tells the screen reader the element is a button, but provides no button functionality; use `button` or `input` with `type="button"`instead.

[`ARIA: cell role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/cell\_role)

identifies an element as being a cell in a tabular container that does not contain column or row header information; the cell must be nested in an element with the role of `row` to be supported

[`ARIA: checkbox role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/checkbox\_role)

checkable, interactive controls; elements containing `role="checkbox"` must also include the [`aria-checked`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-checked) attribute to expose the checkbox's state to assistive technology.

[`ARIA: columnheader role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/columnheader\_role)

identifies an element as being a cell in a row contains header information for a column, similar to the native `th`element with column scope.

[`ARIA: combobox role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/combobox\_role)

identifies an element as an `input` that controls another element, such as a `listbox` or `grid`, that can dynamically pop up to help the user set the value of that `input`.

[`ARIA: command role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/command\_role)

defines a widget that performs an action but does not receive input data.

[`ARIA: comment role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/comment\_role)

semantically denotes a comment/reaction to some content on the page, or to a previous comment.

[`ARIA: complementary role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/complementary\_role)

used to designate a supporting section that relates to the main content, yet can stand alone when separated; frequently presented as sidebars or call-out boxes; if possible, use the [HTML \<aside> element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/aside) instead.

[`ARIA: composite role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/composite\_role)

indicates a widget that may contain navigable descendants or owned children.

[`ARIA: contentinfo role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/contentinfo\_role)

defines a footer, containing identifying information such as copyright information, navigation links, and privacy statements, found on every document within a site; commonly called a footer.

[`ARIA: definition role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/definition\_role)

indicates the element is a definition of a term or concept.

[`ARIA: dialog role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/dialog\_role)

used to mark up an HTML based application dialog or window that separates content or UI from the rest of the web application or page. Dialogs are generally placed on top of the rest of the page content using an overlay. Dialogs can be either non-modal (it's still possible to interact with content outside of the dialog) or modal (only the content in the dialog can be interacted with).

[`ARIA: directory role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/directory\_role)

list of references to members of a group, such as a static table of contents.

[`ARIA: document role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/document\_role)

for focusable content within complex composite [widgets](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/widget\_role) or [applications](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/application\_role) for which assistive technologies can switch reading context back to a reading mode.

[`ARIA: document structural roles`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structural\_roles)

used to provide a structural description for a section of content.

[`ARIA: feed role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/feed\_role)

a dynamic scrollable `list` of `articles` in which articles are added to or removed from either end of the list as the user scrolls. A `feed` enables screen readers to use the browse mode reading cursor to both read and scroll through a stream of rich content that may continue scrolling infinitely by loading more content as the user reads.

[`ARIA: figure role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/figure\_role)

can be used to identify a figure inside page content where appropriate semantics do not already exist; generally considered to be one or more images, code snippets, or other content that puts across information in a different way to a regular flow of text.

[`ARIA: form role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/form\_role)

can be used to identify a group of elements on a page that provide equivalent functionality to an HTML form.

[`ARIA: generic role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/generic\_role)

creates a nameless container element which has no semantic meaning on its own.

[`ARIA: grid role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/grid\_role)

for a widget that contains one or more rows of cells. The position of each cell is significant and can be focused using keyboard input.

[`ARIA: gridcell role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/gridcell\_role)

used to make a cell in a [`grid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/grid\_role) or [`treegrid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treegrid\_role). It is intended to mimic the functionality of the HTML `<td>`element for table-style grouping of information.

[`ARIA: group role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/group\_role)

identifies a set of user interface objects that is not intended to be included in a page summary or table of contents by assistive technologies.

[`ARIA: heading role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/heading\_role)

defines this element as a heading to a page or section, with the [`aria-level`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-level) attribute providing for more structure.

[`ARIA: img role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/img\_role)

identify multiple elements inside page content that should be considered as a single image. These elements could be images, code snippets, text, emojis, or other content that can be combined to deliver information in a visual manner.

[`ARIA: input role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/input\_role)

abstract role; generic type of widget that allows user input.

[`ARIA: landmark role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/landmark\_role)

important subsection of a page; an abstract superclass for the aria role values for sections of content that are important enough that users will likely want to be able to navigate directly to them.

[`ARIA: list role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/list\_role)

a list of items; normally used in conjunction with the `listitem` role, which identifies a list item contained inside the list.

[`ARIA: listbox role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/listbox\_role)

used for lists from which a user may select one or more items which are static and, unlike HTML `select`elements, may contain images.

[`ARIA: listitem role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/listitem\_role)

used to identify an item inside a list of items. It is normally used in conjunction with the [`list`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/list\_role) role, which is used to identify a list container.

[`ARIA: log role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/log\_role)

used to identify an element that creates a [live region](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA\_Live\_Regions) where new information is added in a meaningful order and old information may disappear.

[`ARIA: main role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/main\_role)

primary content of a document. The main content area consists of content that is directly related to or expands upon the central topic of a document, or the main function of an application.

[`ARIA: mark role`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/mark\_role)

denotes content which is marked or highlighted for reference or notation purposes, due to the content's relevance in the enclosing context.

[ARIA: marquee role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/marquee\_role)

type of [live region](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA\_Live\_Regions) containing non-essential information which changes frequently.

[ARIA: math role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/math\_role)

indicates that the content represents a mathematical expression.

[ARIA: menu role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menu\_role)

composite widget that offers a list of choices to the user.

[ARIA: menubar role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menubar\_role)

presentation of `menu` that usually remains visible and is usually presented horizontally.

[ARIA: menuitem role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menuitem\_role)

indicates the element is an option in a set of choices contained by a `menu` or `menubar`.

[ARIA: menuitemcheckbox role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menuitemcheckbox\_role)

a `menuitem` with a checkable state whose possible values are `true`, `false`, or `mixed`.

[ARIA: menuitemradio role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/menuitemradio\_role)

checkable menuitem in a set of elements with the same role, only one of which can be checked at a time.

[ARIA: meter role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/meter\_role)

used to identify an element being used as a meter.

[ARIA: navigation role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/navigation\_role)

used to identify major groups of links used for navigating through a website or page content.

[ARIA: none role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/none\_role)

a synonym for the [`presentation`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/presentation\_role) role; they both remove an element's implicit ARIA semantics from being exposed to the accessibility tree.

[ARIA: note role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/note\_role)

suggests a section whose content is parenthetic or ancillary to the main content.

[ARIA: option role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/option\_role)

used for selectable items in a `listbox`.

[ARIA: presentation role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/presentation\_role)

along with its synonym `none,` removes an element's implicit ARIA semantics from being exposed to the accessibility tree.

[ARIA: progressbar role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/progressbar\_role)

an element that displays the progress status for tasks that take a long time.

[ARIA: radio role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/radio\_role)

group of checkable radio buttons, in a `radiogroup`, where no more than a single radio button can be checked at a time.

[ARIA: radiogroup role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/radiogroup\_role)

a group of `radio`buttons.

[ARIA: range role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/range\_role)

a generic type of structure role representing a range of values.

[ARIA: region role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/region\_role)

used to identify document areas the author deems significant. It is a generic landmark available to aid in navigation when none of the other landmark roles are appropriate.

[ARIA: roletype role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/roletype\_role)

an [abstract role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles#6.\_abstract\_roles), is the base role from which all other ARIA roles inherit.

[ARIA: row role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/row\_role)

a row of cells within a tabular structure. A row contains one or more cells, grid cells or column headers, and possibly a row header, within a [`grid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/grid\_role), [`table`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/table\_role) or [`treegrid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treegrid\_role), and optionally within a [`rowgroup`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/rowgroup\_role).

[ARIA: rowgroup role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/rowgroup\_role)

a group of [rows](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/row\_role) within a tabular structure. A `rowgroup` contains one or more rows of [cells](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/cell\_role), [grid cells](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/gridcell\_role), [column headers](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/columnheader\_role), or [row headers](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/rowheader\_role) within a [`grid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/grid\_role), [`table`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/table\_role) or [`treegrid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treegrid\_role).

[ARIA: rowheader role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/rowheader\_role)

a cell containing header information for a [row](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/row\_role) within a tabular structure of a [`grid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/grid\_role), [`table`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/table\_role) or [`treegrid`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treegrid\_role).

[ARIA: scrollbar role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/scrollbar\_role)

graphical object that controls the scrolling of content within a viewing area.

[ARIA: search role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/search\_role)

used to identify the search functionality; the section of the page used to search the page, site, or collection of sites.

[ARIA: searchbox role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/searchbox\_role)

indicates an element is a type of `textbox` intended for specifying search criteria.

[ARIA: section role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/section\_role)

an abstract role, is superclass role for renderable structural containment components.

[ARIA: sectionhead role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/sectionhead\_role)

an abstract role, is superclass role for labels or summaries of the topic of its related section.

[ARIA: select role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/select\_role)

superclass role for form widgets that allows the user to make selections from a set of choices.

[ARIA: separator role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/separator\_role)

divider that separates and distinguishes sections of content or groups of menu items. The implicit ARIA role the native thematic break `hr` element is `separator`.

[ARIA: slider role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/slider\_role)

input where the user selects a value from within a given range.

[ARIA: spinbutton role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/spinbutton\_role)

type of range that expects the user to select a value from among discrete choices.

[ARIA: status role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/status\_role)

[live region](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA\_Live\_Regions) containing advisory information for the user that is not important enough to be an [`alert`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/alert\_role).

[ARIA: structure role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/structure\_role)

for document structural elements.

[ARIA: suggestion role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/suggestion\_role)

semantically denotes a single proposed change to an editable document. This should be used on an element that wraps an element with an `insertion`role, and one with a `deletion` role.

[ARIA: switch role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/switch\_role)

functionally identical to the [checkbox](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/checkbox\_role) role, except that instead of representing "checked" and "unchecked" states, which are fairly generic in meaning, the `switch` role represents the states "on" and "off."

[ARIA: tab role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tab\_role)

indicates an interactive element inside a `tablist` that, when activated, displays its associated `tabpanel`.

[ARIA: table role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/table\_role)

identifies the element containing the role as having a non-interactive table structure containing data arranged in rows and columns, similar to the native `table` HTML element.

[ARIA: tablist role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tablist\_role)

identifies element that serves as the container for a set of `tabs (tabpanel` elements).

[ARIA: tabpanel role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tabpanel\_role)

container for the resources of layered content associated with a `tab`.

[ARIA: term role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/term\_role)

can be used for a word or phrase with an optional corresponding [`definition`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/definition\_role).

[ARIA: textbox role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/textbox\_role)

identifies an element that allows the input of free-form text. Whenever possible, rather than using this role, use an `input` element with [type="text"](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/text), for single-line input, or a `textarea` element for multi-line input.

[ARIA: timer role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/timer\_role)

indicates to assistive technologies that an element is a numerical counter listing the amount of elapsed time from a starting point or the remaining time until an end point. Assistive technologies will not announce updates to a timer as it has an implicit [aria-live](https://www.w3.org/TR/wai-aria/#aria-live) value of `off`.

[ARIA: toolbar role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/toolbar\_role)

defines containing element as a collection of commonly used function buttons or controls represented in a compact visual form.

[ARIA: tooltip role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tooltip\_role)

contextual text bubble that displays a description for an element that appears on pointer hover or keyboard focus.

[ARIA: tree role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tree\_role)

widget that allows the user to select one or more items from a hierarchically organized collection.

[ARIA: treegrid role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treegrid\_role)

identifies an element as being grid whose rows can be expanded and collapsed in the same manner as for a `tree`.

[ARIA: treeitem role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/treeitem\_role)

an item in a `tree`.

