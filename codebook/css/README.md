---
description: CSS Properties
---

# 💎 CSS

### `background-attachment` <a href="#syntax" id="syntax"></a>

`fixed` - background is fixed relative to the viewport. Even if an element has a scrolling mechanism, the background doesn't move with the element.

{% hint style="info" %}
This is not compatible with `background-clip: text`
{% endhint %}

`local` - background is fixed relative to the element's contents. If the element has a scrolling mechanism, the background scrolls with the element's contents, and the background painting area and background positioning area are relative to the scrollable area of the element rather than to the border framing them.

`scroll` - background is fixed relative to the element itself and does not scroll with its contents. (It is effectively attached to the element's border.)is fixed relative to the viewport. Even if an element has a scrolling mechanism, the background doesn't move with the element.&#x20;

{% hint style="info" %}
This is not compatible with `background-clip: text`
{% endhint %}

`local`- background is fixed relative to the element's contents. If the element has a scrolling mechanism, the background scrolls with the element's contents, and the background painting area and background positioning area are relative to the scrollable area of the element rather than to the border framing them.

`scroll` - background is fixed relative to the element itself and does not scroll with its contents. (It is effectively attached to the element's border.)

#### Syntax <a href="#syntax" id="syntax"></a>

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```css
/* Keyword values */
background-attachment: scroll;
background-attachment: fixed;
background-attachment: local;

/* Global values */
background-attachment: inherit;
background-attachment: initial;
background-attachment: revert;
background-attachment: revert-layer;
background-attachment: unset;
```
{% endcode %}

#### Example <a href="#simple_example" id="simple_example"></a>

<pre class="language-html" data-overflow="wrap" data-line-numbers data-full-width="true"><code class="lang-html"><strong>&#x3C;style>
</strong>p {
  background-image: url("starsolid.gif");
  background-attachment: fixed;
}
&#x3C;/style>

<strong>&#x3C;p>
</strong>  There were doors all round the hall, but they were all locked; and when Alice had been all the way down one side and up the other, trying every door, she walked sadly down the middle, wondering how she was ever to get out again.
&#x3C;/p>
</code></pre>

#### Multiple background images <a href="#multiple_background_images" id="multiple_background_images"></a>

This property supports multiple background images. You can specify a different `<attachment>` for each background, separated by commas. Each image is matched with the corresponding `<attachment>` type, from first specified to last.

**Example**

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<style>
p {
  background-image: url("starsolid.gif"), url("startransparent.gif");
  background-attachment: fixed, scroll;
  background-repeat: no-repeat, repeat-y;
}
</style>

<p>
  There were doors all round the hall, but they were all locked; and when Alice had been all the way down one side and up the other, trying every door, she walked sadly down the middle, wondering how she was ever to get out again.
  Suddenly she came upon a little three-legged table, all made of solid glass; there was nothing on it except a tiny golden key, and Alice's first thought was that it might belong to one of the doors of the hall; but, alas! either the locks were too large, or the key was too small, but at any rate it would not open any of them. However, on the second time round, she came upon a low curtain she had not noticed before, and behind it was a little door about fifteen inches high: she tried the little golden key in the lock, and to her great delight it fitted!
</p>
```
{% endcode %}

### `border-style`

<pre class="language-css" data-overflow="wrap" data-line-numbers data-full-width="true"><code class="lang-css"><strong>border-style: none;
</strong>border-style: hidden;
border-style: dotted;
border-style: dashed;
border-style: solid;
border-style: double;
border-style: groove;
border-style: ridge;
border-style: inset;
border-style: outset;

top and bottom | left and right
border-style: dotted solid;

top | left and right | bottom
border-style: hidden double dashed;

top | right | bottom | left
border-style: none solid dotted dashed;

/* Global values */
border-style: inherit;
border-style: initial;
border-style: revert;
border-style: revert-layer;
border-style: unset;
</code></pre>

#### Example

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<pre class="b1">none</pre>
<pre class="b2">hidden</pre>
<pre class="b3">dotted</pre>
<pre class="b4">dashed</pre>
<pre class="b5">solid</pre>
<pre class="b6">double</pre>
<pre class="b7">groove</pre>
<pre class="b8">ridge</pre>
<pre class="b9">inset</pre>
<pre class="b10">outset</pre>
```
{% endcode %}

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```css
  pre {
  height: 80px;
  width: 120px;
  margin: 20px;
  padding: 20px;
  display: inline-block;
  background-color: palegreen;
  border-width: 5px;
  box-sizing: border-box;
}

.b1 {
  border-style: none;
}

.b2 {
  border-style: hidden;
}

.b3 {
  border-style: dotted;
}

.b4 {
  border-style: dashed;
}

.b5 {
  border-style: solid;
}

.b6 {
  border-style: double;
}

.b7 {
  border-style: groove;
}

.b8 {
  border-style: ridge;
}

.b9 {
  border-style: inset;
}

.b10 {
  border-style: outset;
}
```
{% endcode %}

### `list-style`

`list-style` is shorthand for the following properties:

* `list-style-type`
* `list-style-position`&#x20;
* `list-style-image`

If one of the values are missing, the default value for that property will be used.

Default value: `disc outside none`

#### Example

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```css
list-style: disc inside;
list-style: disc outside;
list-style: circle inside;
list-style: circle outside;
list-style: square inside;
list-style: upper-roman inside;
list-style: lower-alpha inside;
list-style: disc inside url("sqpurple.gif");
list-style: disc inside url("smiley.gif");
```
{% endcode %}

### `text-decoration`

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<!DOCTYPE html>
<html>
<head>
<style>
h1 {
  text-decoration-line: underline;
  text-decoration-style: solid;
}

h2 {
  text-decoration-line: underline;
  text-decoration-style: double;
}

h3 {
  text-decoration-line: underline;
  text-decoration-style: dotted;  
}

p.ex1 {
  text-decoration-line: underline;
  text-decoration-style: dashed;  
}

p.ex2 {
  text-decoration-line: underline;
  text-decoration-style: wavy;  
}

p.ex3 {
  text-decoration-line: underline;
  text-decoration-color: red;  
  text-decoration-style: wavy;  
}
</style>
</head>

<body>
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<p class="ex1">A paragraph.</p>
<p class="ex2">Another paragraph.</p>
<p class="ex3">Another paragraph.</p>
</body>
</html>
```
{% endcode %}
