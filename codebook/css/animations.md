---
layout:
  title:
    visible: true
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 📟 Animations

### `animation`

|                             | ​                                                                                                                          |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| `animation-name`            | name given element receiving animation                                                                                     |
| `animation-duration`        | how long an animation should take to complete; if not specified, animation will not occur; default value is 0s (0 seconds) |
| `animation-delay`           | delays start                                                                                                               |
| `animation-iteration-count` | number of times animation should run                                                                                       |
| `animation-direction`       | plays forward, backwards or in alternate cycles                                                                            |
| `animation-timing-function` | speed curve of the animation                                                                                               |
| `animation-fill-mode`       | style for the target element when the animation is not playing (before it starts, after it ends, or both)                  |

#### `animation-direction` <a href="#animation-direction" id="animation-direction"></a>

`normal` - default; normal (forwards)

`reverse`- reverse direction (backwards)

`alternate` - forwards first, then backwards

`alternate-reverse` - backwards first, then forwards

#### `animation-timing-function` <a href="#animation-timing-function" id="animation-timing-function"></a>

`ease` - default; slow start, then fast, then end slowly

`linear` - same speed from start to end

`ease-in` - slow start

`ease-out` - slow end

`ease-in-out` - slow start and end

`cubic-bezier(n,n,n,n)` - choose values in a `cubic-bezier` function

**Example**

```html
<!DOCTYPE html>
<html>
<head>
<style>
div {
width: 100px;
height: 50px;
background-color: red;
font-weight: bold;
position: relative;
animation: mymove 5s infinite;
}
#div1 {
animation-timing-function: linear;
}
#div2 {
animation-timing-function: ease;
}
#div3 {
animation-timing-function: ease-in;
}
#div4 {
animation-timing-function: ease-out;
}
#div5 {
animation-timing-function: ease-in-out;
}
@keyframes mymove {
from {
left: 0px;
}
to {
left: 300px;
 }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<p>The animation-timing-function property specifies the speed curve of the animation. The following example shows some of the different speed curves that can be used:</p>
<div id="div1">linear</div>
<div id="div2">ease</div>
<div id="div3">ease-in</div>
<div id="div4">ease-out</div>
<div id="div5">ease-in-out</div>
</body>
</html>
```

### `animation-fill-mode`

`none` - default; animation will not apply any styles to element before or after it is executing

`forwards` - element retains style values set by last keyframe (depends on `animation-direction` and `animation-iteration-count`)

`backwards` - element gets style values set by first keyframe (depends on `animation-direction`), and retain this during `animation-delay` period

`both` - both forwards and backwards, extending the animation properties in both directions

#### Animation Shorthand&#x20;

**Example**

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 100px;
  background-color: red;
  position: relative;
  animation-name: example;
  animation-duration: 5s;
  animation-timing-function: linear;
  animation-delay: 2s;
  animation-iteration-count: infinite;
  animation-direction: alternate;
}

@keyframes example {
0%   {
  background-color:red; 
  left:0px; 
  top:0px;
}
  25%  {
  background-color: yellow; 
  left:200px; 
  top:0px;
}
50%  {
  background-color:blue;   
  left:200px; 
  top:200px;
}
75%  {
  background-color:green;
  left:0px; 
  top:200px;
}
  100% {
  background-color:red; 
  left:0px; 
  top:0px;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<p>This example uses six of the animation properties:</p>
<div></div>
</body>
</html>
```
{% endcode %}

**Shorthand**

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 100px;
  background-color: red;
  position: relative;
  animation: myfirst 5s linear 2s infinite alternate;
}

@keyframes myfirst {
  0%   {
  background-color:red; 
  left:0px; 
  top:0px;
}
25%  {
  background-color:yellow; 
  left:200px; 
  top:0px;
}
50%  {
  background-color:blue; 
  left:200px;
  top:200px;
}
75%  {
  background-color:green;
  left:0px;
  top:200px;
}
100% {
  background-color:red; 
  left:0px;
  top:0px;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<p>This example uses the shorthand animation property:</p>
<div></div>
</body>
</html>
```
{% endcode %}
