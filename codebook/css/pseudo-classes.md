# 🧴 Pseudo-classes



<table><thead><tr><th width="800">Pseudoclass</th><th width="800">Description</th><th data-hidden></th></tr></thead><tbody><tr><td>:active</td><td>when an element reaches the active state, its style is updated/changed</td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr></tbody></table>

{% embed url="https://codecademy.com" %}

***

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th data-type="number"></th><th data-type="checkbox"></th><th data-type="select"></th><th data-type="users" data-multiple></th><th data-type="users" data-multiple></th><th data-type="files"></th><th data-type="content-ref"></th><th data-type="rating" data-max="5"></th></tr></thead><tbody><tr><td>:active</td><td>null</td><td>true</td><td></td><td></td><td></td><td></td><td></td><td>null</td></tr><tr><td>Text</td><td>0</td><td>false</td><td></td><td></td><td></td><td></td><td></td><td>2</td></tr><tr><td></td><td>null</td><td>true</td><td></td><td></td><td></td><td></td><td></td><td>null</td></tr><tr><td></td><td>null</td><td>true</td><td></td><td></td><td></td><td></td><td></td><td>null</td></tr></tbody></table>

***

*

{% tabs %}
{% tab title="Second Tab" %}
```
when an element reaches the active state, its style is updated/changed
```
{% endtab %}
{% endtabs %}
