# 📼 Transitions

## Transitions

### Transitions <a href="#transitions" id="transitions"></a>

| Name                         | Description                                                                                      |
| ---------------------------- | ------------------------------------------------------------------------------------------------ |
| `transition`                 | shorthand property for setting all four individual transition properties in a single declaration |
| `transition-delay`           | when transition starts                                                                           |
| `transition-duration`        | number of seconds or milliseconds a transition animation should take to complete                 |
| `transition-property`        | names of the CSS properties to which a transition effect should be applied to                    |
| `transition-timing-function` | how intermediate values of CSS properties are being affected by a transition will be calculated  |

> For Safari browser, you should use the `-webkit-transition-` property to specify which properties should be animated and the `-webkit-transition-duration` for the duration

For `transition` to work, the CSS property and duration of the effect must be provided.

**Note: the default duration is 0.**

**Example:**

**CSS**

button {2color: white;3background: purple;4transition: background 0.3s;5-webkit-transition-property: background;6-webkit-transition-duration: 0.3s;7}8​9button:hover {10background: black;11}

### Keyframes <a href="#keyframes" id="keyframes"></a>

* lets an element gradually change from one style to another
* can change as many CSS properties you want, as many times as you want
* must first specify some keyframes for the animation
* keyframes hold what styles the element will have at certain times

#### The `@keyframes` Rule <a href="#the-keyframes-rule" id="the-keyframes-rule"></a>

When you specify CSS styles inside the @keyframes rule, the animation will gradually change from the current style to the new style at certain times. To get an animation to work, you must bind the animation to an element.

**Example 1**

* binds the "example" animation to `<div>`
* animation will last for 4 seconds
* gradually changes background-color of `<div>` from "red" to "yellow"
* specifies when the style will change by using keywords "from" and "to" (start to complete)

**HTML**1

\<!DOCTYPE html>2\<html>3\<head>4\<style>5div {6width: 100px;7height: 100px;8background-color: red;9animation-name: example;10animation-duration: 4s;11}12​13@keyframes example {14from {15background-color: red;16}17​18to {19background-color: yellow;20}21}22\</style>23\</head>24\<body>25\<h1>CSS Animation\</h1>26\<div>\</div>27\<p>\<b>Note:\</b> When an animation is finished, it goes back to its original style.\</p>28\</body>29\</html>



**Example 2**

* may also use percent
* 0% (start) and 100% (complete)
* using percent allows the use of as many style changes as you like
* will change `background-color` of `<div>` when animation is 25% complete, 50% complete, and 100% complete

**HTML**1\<!DOCTYPE html>2\<html>3\<head>4\<style>5div {6width: 100px;7height: 100px;8background-color: red;9animation-name: example;10animation-duration: 4s;11}12​13@keyframes example {140% {15background-color: red;16}17​1825% {19background-color: yellow;20}21​2250% {23background-color: blue;24}25​26100% {27background-color: green;28}29}30\</style>31\</head>32\<body>33\<h1>CSS Animation\</h1>34\<div>\</div>35\<p>\<b>Note:\</b> When an animation is finished, it goes back to its original style.\</p>36\</body>37\</html>**Example 3**

* both `background-color` and `position` of `<div>` change when the animation is 25% complete, 50% complete, and 100% complete

**HTML**1\<!DOCTYPE html>2\<html>3\<head>4\<style>5div {6width: 100px;7height: 100px;8background-color: red;9position: relative;10animation-name: example;11animation-duration: 4s;12}13​14@keyframes example {150% {16background-color:red;17left:0px;18top:0px;19}20​2125% {22background-color:yellow;23left:200px;24top:0px;25}26​2750% {28background-color:blue;29left:200px;30top:200px;31}32​3375% {34background-color:green;35left:0px;36top:200px;37}38​39100% {40background-color:red;41left:0px;42top:0px;43}44}45\</style>46\</head>47\<body>48\<h1>CSS Animation\</h1>49\<div>\</div>50\<p>\<b>Note:\</b> When an animation is finished, it goes back to its original style.\</p>51\</body>52\</html>
