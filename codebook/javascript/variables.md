# 📲 Variables

## Variables

* Variables are named labels/identifiers for storing values

**There are three ways to declare/create a variable in Javascript**

`var`

* The most common variable. It can be reassigned but only accessed within a function. Variables defined with `var` move to the top when the code is executed. **(don’t use anymore)**

`let`

* Similar to const, the let variable can be reassigned but not re-declared.

`const`

* Can not be reassigned and not accessible before they appear within the code.

`var`, `let`, and `const` are reserved keywords in Javascript. You can’t use them for anything other than declaring variables.

**Numbers**

* **34**
* **10** whole number (integer)
* **3.14** floating point number

**Strings (used for text)**

* “Tom” double quotes
* ‘Tom’ single quotes
* \`Tom\` back ticks (used for string interpolation)

**Booleans**

* true or false values

**Undefined**

* undefined a variable has been declared but no value has been assigned to it yet

**Null**

* null means there explicitly is no value

### Arithmetic Operators

**Description**

Addition

Subtraction

Multiplication

Exponent

Division

Modulus (returns the remainder)

**Operator**

`+`

`-`

`*`

`**`

`/`

`%`

**Examples**

`1 + 1 //-> 2`

`13 - 2 //-> 1`

`5 * 3 //-> 15`

`3 ** 2` ('3 to the power of 2')

`10 / 2 //-> 5`

`10 % 3 //-> 1`

![Arithmetic operators table from Code Platoon](https://cdn.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/https://www.filepicker.io/api/file/ai1NizUZTQJRGu3sWZto)

### Logic Operators

![Logical operators table from Code Platoon](https://cdn.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/https://www.filepicker.io/api/file/8vahoXC3TjmKTqSjyDNa)

**Description**

Equal

Not equal

Greater than

Less than

Greater than or equal

Less than or equal to

**Operator**

`==` **or** `===`

`!=` **or** `!==`

`>`

`<`

`>=`

`<=`

**Examples**

`let a = 4` `let b = 2`

`a == b //-> false`

`a !== b //-> true`

`a > b //-> true`

`a < b //-> false`

`a >= b //-> true`

`a <= b //-> false`

### AND / OR Operators

![AND/OR table from Code Platoon](https://cdn.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/https://www.filepicker.io/api/file/5VPwM5fSyenwR0n4v5Qg)

**Description**

Logical AND

Logical OR

**Operator**

`&&`

`||`

**Examples**

`let a = 4` `let b = 2` `let c = 8`

`a > b && b < c //-> true`

`a > c && a > b // -> false`

`a > b || b < c //-> true`

`a > c || a > b //-> true`

### Mathematical Assignment Operators

Variable value is updated _and_ assigned as the new value of that variable.

<pre class="language-javascript" data-overflow="wrap" data-line-numbers data-full-width="true"><code class="lang-javascript">x += 
x -=
x *=
x /=
x++
<strong>x--
</strong></code></pre>

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```javascript
let w = 4;
w += 1;
console.log(w); // Output: 5
// is equivalent to:
let w = 4;
w = w + 4;
console.log(w); //Output: 5 
```
{% endcode %}

* The most common variable. It can be reassigned but only accessed within a function. Variables defined with `var` move to the top when the code is executed. **(don’t use anymore)**

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```javascript
let x = 20;
x -= 5; // Can be written as x = x - 5
console.log(x); // Output: 15

let y = 50;
y *= 2; // Can be written as y = y * 2
console.log(y); // Output: 100

let z = 8;
z /= 2; // Can be written as z = z / 2
console.log(z); // Output: 4
```
{% endcode %}

#### The Increment and Decrement Operator

increment operator: `++`

\-increases variable value by 1

\
decrement operator: `--`

\-decreases variable value by 1

#### String Concatenation with Variables

The `+` operator can be used to combine two string values even if those values are being stored in variables:

{% code overflow="wrap" lineNumbers="true" %}
```javascript
let myPet = 'armadillo';console.log('I own a pet ' + myPet + '.'); // Output: 'I own a pet armadillo.'
```
{% endcode %}

In the example above, we assigned the value `'armadillo'` to the `myPet`variable. On the second line, the `+` operator is used to combine three strings: `'I own a pet'`, the value saved to `myPet`, and `'.'`. We log the result of this concatenation to the console as:

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```javascript
I own a pet armadillo.
```
{% endcode %}

#### String Interpolation

In the ES6 version of JavaScript, we can insert, or _interpolate_, variables into strings using _template literals_. Check out the following example where a template literal is used to log strings together:

{% code overflow="wrap" lineNumbers="true" fullWidth="true" %}
```javascript
const myPet = 'armadillo';console.log(`I own a pet ${myPet}.`);// Output: I own a pet armadillo.
```
{% endcode %}

Notice that:

* a template literal is wrapped by backticks `` ` ``(this key is usually located on the top of your keyboard, left of the 1 key).
* Inside the template literal, you’ll see a placeholder, `${myPet}`. The value of `myPet` is inserted into the template literal.
* When we interpolate `` `I own a pet ${myPet}.` ``, the output we print is the string: `'I own a pet armadillo.'`
