# Table of contents

* [💽 CodeBook](README.md)
  * [📼 JavaScript](codebook/javascript/README.md)
    * [📲 Variables](codebook/javascript/variables.md)
    * [🔦 Functions](codebook/javascript/functions.md)
    * [💾 Conditional Statements](codebook/javascript/conditional-statements.md)
  * [💎 CSS](codebook/css/README.md)
    * [🧴 Pseudo-classes](codebook/css/pseudo-classes.md)
    * [🧻 Pseudo-elements](codebook/css/pseudo-elements.md)
    * [📟 Animations](codebook/css/animations.md)
    * [📼 Transitions](codebook/css/transitions.md)
    * [Page 1](codebook/css/page-1.md)
  * [🔮 HTML](codebook/html/README.md)
    * [🩹 WAI-ARIA Roles](codebook/html/wai-aria-roles.md)
    * [🧂 Elements](codebook/html/elements.md)
    * [Element Categories](codebook/html/element-categories.md)
    * [Attributes](codebook/html/attributes.md)

## Group 1

* [Functions](group-1/functions.md)
